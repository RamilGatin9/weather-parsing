package com.company;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class WeatherManager {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the path to file");
        String path = scanner.nextLine();

        WeatherManager weatherManager = new WeatherManager();

        File file = new File(path);

        weatherManager.weatherData(file);
    }

    File weatherData(File file) throws IOException {

        File weatherInformation = new File(file.getParent() + "\\answer.txt");

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader((fileReader));

        ArrayList<Weather> weathers = new ArrayList<>();


        for (int i = 0; i < 10; i++) {
            bufferedReader.readLine();
        }
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {

            Integer year = Integer.valueOf(line.substring(0, 4));
            line = line.substring(4);

            Integer month = Integer.valueOf(line.substring(0, 2));
            line = line.substring(2);

            Integer day = Integer.valueOf(line.substring(0, 2));
            line = line.substring(2);

            Integer hour = Integer.valueOf(line.substring(1, 3));
            line = line.substring(6);

            Float temperature = Float.valueOf(line.substring(0, line.indexOf(',')));
            line = line.substring(line.indexOf(',') + 1);

            Float relativeHumidity = Float.valueOf(line.substring(0, line.indexOf(',')));
            line = line.substring(line.indexOf(',') + 1);

            Float windSpeed = Float.valueOf(line.substring(0, line.indexOf(',')));
            line = line.substring(line.indexOf(',') + 1);

            Float windDirection = Float.valueOf(line);

            Weather weather = new Weather(year, month, day, hour, temperature, relativeHumidity, windSpeed, windDirection);
            weathers.add(weather);

        }


        bufferedReader.close();
        fileReader.close();


        weatherInformation.createNewFile();
        FileWriter fileWriter = new FileWriter(weatherInformation);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write("Average temperature is:" + findTheAverageTemperature(weathers) );
        bufferedWriter.newLine();
        bufferedWriter.write("Average relative humidity is:" + findTheAverageRelativeHumidity(weathers) );
        bufferedWriter.newLine();
        bufferedWriter.write("Average wind speed is:" + findTheAverageWindSpeed(weathers) );
        bufferedWriter.newLine();
        bufferedWriter.write(findTheHighestTemperature(weathers) );
        bufferedWriter.newLine();
        bufferedWriter.write("The lowest relative humidity is:" + findLowestRelativeHumidity(weathers) );
        bufferedWriter.newLine();
        bufferedWriter.write("The highest wind speed is:" + findTheHighestWindSpeed(weathers) );
        bufferedWriter.newLine();
        bufferedWriter.write(windDirection(weathers));
        bufferedWriter.close();
        fileWriter.close();

        return weatherInformation;
    }

    Float findTheAverageTemperature(ArrayList<Weather> weathers) {
        float result = 0;

        for (Weather weather : weathers) {
            result += weather.getTemperature();
        }
        return result / weathers.size();
    }

    Float findTheAverageRelativeHumidity(ArrayList<Weather> weathers) {
        float result = 0;
        for (Weather weather : weathers) {
            result += weather.getRelativeHumidity();
        }
        return result / weathers.size();
    }

    Float findTheAverageWindSpeed(ArrayList<Weather> weathers) {
        float result = 0;
        for (Weather weather : weathers) {
            result += weather.getWindSpeed();
        }
        return result / weathers.size();
    }

    String findTheHighestTemperature(ArrayList<Weather> weathers) {
        float result = 0;
        int day = 0;
        int hour = 0;
        for (Weather weather : weathers) {
            if (weather.getTemperature() > result) {
                result = weather.getTemperature();
                day = weather.getDay();
                hour = weather.getHour();
            }
        }
        return "The highest temperature is : " + result + "  On this day : 2021.03." + day + "  at :" + hour + ".00";
    }

    Float findLowestRelativeHumidity(ArrayList<Weather> weathers) {
        float max = Float.MAX_VALUE;

        for (Weather weather : weathers) {
            if (weather.getRelativeHumidity() < max) {
                max = weather.getRelativeHumidity();
            }
        }
        return max;
    }

    Float findTheHighestWindSpeed(ArrayList<Weather> weathers) {
        float result = 0;
        for (Weather weather : weathers) {
            if (result < weather.getWindSpeed()) {
                result = weather.getWindSpeed();
            }
        }
        return result;
    }

    String windDirection(ArrayList<Weather> weathers) {

        int n = 0;
        int s = 0;
        int w = 0;
        int e = 0;
        for (Weather weather : weathers) {
            if (weather.getWindDirection() >= 45 && weather.getWindDirection() < 135) {
                e++;
            } else if (weather.getWindDirection() >= 135 && weather.getWindDirection() < 225) {
                s++;
            } else if (weather.getWindDirection() >= 225 && weather.getWindDirection() < 315) {
                w++;
            } else {
                n++;
            }
        }

        if (e > n && e > s && e > w) {
            return "Most often the wind blows towards the East";
        }
        if (s > n && s > w && s > e) {
            return "Most often the wind blows towards the South";
        }

        if (w > e && w > n && w > s) {
            return "Most often the wind blows towards the West";
        }

        if (n > s && n > w && n > e) {
            return "Most often the wind blows towards the North";
        }

        return "False";
    }
}
